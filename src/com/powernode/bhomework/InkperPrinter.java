package com.powernode.bhomework;

/**
 * 描述 喷墨式打印机
 * 作者 张明阳
 * 时间 2023/12/12 19:16
 */
public class InkperPrinter extends Printer{

    @Override
    public void print() {
        System.out.println("打印效果介于针式和激光打印机之间");
    }
}
