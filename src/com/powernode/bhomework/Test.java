package com.powernode.bhomework;

/**
 * 描述 永无bug
 * 作者 张明阳
 * 时间 2023/12/12 19:21
 */
public class Test {
    public static void main(String[] args) {
        Printer pr1 = new DoMatriPrinter();
        Printer pr2 = new InkperPrinter();
        Printer pr3 = new LaserPrinter();
        pr1.print();
        pr2.print();
        pr3.print();
    }
}
