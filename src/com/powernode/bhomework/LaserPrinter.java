package com.powernode.bhomework;

/**
 * 描述 激光打印机
 * 作者 张明阳
 * 时间 2023/12/12 19:18
 */
public class LaserPrinter extends Printer{

    @Override
    public void print() {
        System.out.println("打印速度快，噪音小，效果好");
    }
}
