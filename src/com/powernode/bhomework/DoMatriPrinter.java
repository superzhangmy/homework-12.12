package com.powernode.bhomework;

/**
 * 描述 针式打印机
 * 作者 张明阳
 * 时间 2023/12/12 19:16
 */
public class DoMatriPrinter extends Printer {

    @Override
    public void print() {
        System.out.println("打印速度慢，效果差，噪音高");
    }
}
