package com.powernode.bhomework;

/**
 * 描述 打印机类
 * 作者 张明阳
 * 时间 2023/12/12 19:07
 */
public abstract class Printer {
    public abstract void print();
}
