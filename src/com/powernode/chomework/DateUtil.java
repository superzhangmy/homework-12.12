package com.powernode.chomework;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * 描述 工具类
 * 作者 张明阳
 * 时间 2023/12/12 19:36
 */
public class DateUtil {
    /**
     * 字符串转日期
     * @param s
     */
    public void convert(String s) throws ParseException {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:SS");
        Date parse = sdf.parse(s);
        System.out.println(parse);
    }

    /**
     * 日期转字符串
     * @param date
     */
    public String convert(Date date) {
        SimpleDateFormat date1=new SimpleDateFormat("yyyy-MM-dd HH:mm:SS");
        String str=date1.format(date);
        System.out.println(str);
        return str;
    }

}
