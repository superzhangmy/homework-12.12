package com.powernode.ahomework;

import java.io.*;
import java.util.ArrayList;

/**
 * 描述 永无bug
 * 作者 张明阳
 * 时间 2023/12/12 18:32
 */
public class BHomework {
    public static void main(String[] args) {
        try (
                FileReader fr = new FileReader("I:\\作业\\homework\\code\\04-homework-12.12\\dex\\1.TXT");
                FileWriter fw = new FileWriter("I:\\作业\\homework\\code\\04-homework-12.12\\dex\\3.TXT");
                BufferedReader br = new BufferedReader(fr);
                BufferedWriter bw = new BufferedWriter(fw);
        ) {
            String s = null;
            ArrayList<Object> objArr = new ArrayList<>();
            while ((s= br.readLine())!=null){
                objArr.add(s);
                bw.write(s);
                bw.newLine();
            }
            int num=0;
            for (Object obj:objArr) {
                if (obj.toString().contains("a")) {
                    System.out.println(obj);
                    num++;
                }
            }
            System.out.println("一共有"+num+"个");

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
