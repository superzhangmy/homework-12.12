package com.powernode.ahomework;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

/**
 * 描述 永无bug
 * 作者 张明阳
 * 时间 2023/12/12 18:11
 */
public class AHomework {
    public static void main(String[] args) {
        try (
                FileReader fr = new FileReader("I:\\作业\\homework\\code\\04-homework-12.12\\dex\\1.TXT");
                FileWriter fw = new FileWriter("I:\\作业\\homework\\code\\04-homework-12.12\\dex\\2.TXT")
        ) {
            int num = -1;
            while ((num = fr.read()) != -1) {
                fw.write(num);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
