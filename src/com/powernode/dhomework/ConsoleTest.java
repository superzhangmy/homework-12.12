package com.powernode.dhomework;

import java.util.Scanner;

/**
 * 描述 控制台
 * 作者 张明阳
 * 时间 2023/12/12 20:13
 */
public class ConsoleTest {
    public static void main(String[] args) {
        //键盘录入信息
        UserService ct = new UserService();
        Scanner input = new Scanner(System.in);
        for (int i = 1; i <= 3; i++) {
            System.out.print("请输入账号: ");
            String username = input.next();
            System.out.print("请输入密码: ");
            String userPassword = input.next();
            boolean flag = ct.login(username, userPassword);
            if (flag) {
                System.out.println("欢迎admin登录");
                System.exit(0);
            }else {
                if (i==3){
                    System.out.println("用户名或密码错误，今日机会已用完，请明天再试");
                    System.exit(0);
                }
                System.out.println("用户名或密码错误，你还剩"+(3-i)+"次机会");
            }

        }
    }
}
